# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.2] 2019-05-08
### Added
- CLI tooling container post
- Temporary about page

### Changed
- Title of weblog
- hugo-coder theme now forked
- Photo on front page

## [0.0.1] 2019-04-01
### Added
- Netlify.toml to configure netlify settings
- This CHANGELOG
- A README
- The MIT license

### Changed 

<!--
## [0.0.0] - 2017-06-20
### Added
### Changed
### Fixed
### Removed
-->

[Unreleased]: https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...HEAD
[0.0.1]: https://github.com/olivierlacan/keep-a-changelog/releases/tag/v0.0.1
